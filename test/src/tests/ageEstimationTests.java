package tests;

import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.Test;

import com.biometry.AgeFinder;
import com.biometry.AgeStat.BioParam;

public class ageEstimationTests extends TestCase {

	AgeFinder ageFinder = new AgeFinder();
	

	@Test
	public void testAgeEstimate() {
		
		double age = ageFinder.estimateAge(8.1, 29.7, 28.6, 6.2);
		assertEquals(age, 32.0);
	}
	
	@Test
	public void testBpdAgeEstimates() {
		double bpdAge = ageFinder.findAge(8.1, BioParam.BPD);
		assertEquals(bpdAge,32.0);				
	}

	@Test
	public void testHCAgeEstimates() {
		double hcAge = ageFinder.findAge(29.7, BioParam.HC);
		assertEquals(hcAge,31.5);				
	}
	@Test
	public void testACAgeEstimates() {
		double acAge = ageFinder.findAge(28.6, BioParam.AC);
		assertEquals(acAge,32.5);				
	}
	@Test
	public void testFLAgeEstimates() {
		double flAge = ageFinder.findAge(6.2, BioParam.FL);
		assertEquals(flAge,32.0);				
	}

	
	
}
