package tests;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

import com.biometry.CalcResult;
import com.biometry.FetalBiometry;
import com.biometry.IFetalBiometry;
import com.biometry.MetaCalc;

public class MetaCalcTests extends TestCase {

	@Test
	public void testMetaCalc() {

		IFetalBiometry b = new FetalBiometry();
		List<MetaCalc> calcs = MetaCalc.getCalcs(b);

		MetaCalc c = calcs.get(1);
		System.out.println("methodName " + c.getMethodName());
		assertEquals(c.getMethodName(), "computeHC");

		CalcResult r = b.performCalc(21 * 7, 200.0d,c);
		assertEquals(0.8643, r.getPercentile(), 0.001);
	}

	@Test	
	public void testMetaCalcList() {
		IFetalBiometry b = new FetalBiometry();
		List<MetaCalc> calcs = MetaCalc.getCalcs(b);
		
		for (MetaCalc c : calcs) {

			CalcResult r = b.performCalc(21 * 7, 200.0d,c);
			System.out.println("method: " + c.getMethodName() + "expected mean:" + r.getValue());
			assert(null != r);
		}
	}
	
	
	@Test
	public void testSerialization() {

		IFetalBiometry b = new FetalBiometry();
		List<MetaCalc> calcs = MetaCalc.getCalcs(b);
		ObjectOutputStream oos;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baos);
			oos.writeObject(calcs.get(1));

			ByteArrayInputStream bis = new ByteArrayInputStream(
					baos.toByteArray());

			ObjectInputStream ois = new ObjectInputStream(bis);
			MetaCalc newcalc = (MetaCalc) ois.readObject();

			assertEquals(newcalc.getMethodName(), calcs.get(1).getMethodName());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// FileOutputStream fos = new FileOutputStream(System.);
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// StringOutputStream os = new StringOutputStream();

	}

}
