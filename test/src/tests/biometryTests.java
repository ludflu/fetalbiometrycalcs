package tests;

import junit.framework.TestCase;

import org.junit.Test;

import com.biometry.CalcResult;
import com.biometry.FetalBiometry;
import com.biometry.IFetalBiometry;


public class biometryTests extends TestCase {
	
IFetalBiometry fb = new FetalBiometry();

	@Test
	public void testWeight() {
		CalcResult result = fb.computeEFW(21*7,400.0); //21 weeks, 0 days, 400 grams
		System.out.println("grams:" + result.getValue());		
		System.out.println("weight percentile:" + result.getPercentile());
		assertEquals(result.getPercentile(), 0.508,.01);

	}

	@Test
	public void testHeadCircumference() {
		CalcResult result = fb.computeHC(21*7,200.00); //21 weeks, 0 days, 200 mm
		System.out.println("Head Circ mm:" + result.getValue());		
		System.out.println("HC percentile:" + result.getPercentile());	
		assertEquals(result.getPercentile() ,0.8599,.01);
	}

	@Test
	public void testHeadCircumference2() {
		CalcResult result = fb.computeHC(25*7,235.00); //25 weeks, 0 days, 235 mm
		System.out.println("Head Circ mm:" + result.getValue());		
		System.out.println("HC percentile:" + result.getPercentile());	
		assertEquals( result.getPercentile(),0.50,.0001);
	}

	
	@Test
	public void testFemurLength() {
		CalcResult result = fb.computeFL(21*7,35.6);
		System.out.println("mm:" + result.getValue());
		System.out.println("Femur Length percentile:" + result.getPercentile());
		assertEquals(result.getPercentile() ,0.5,.01);
		}
	
	@Test
	public void testAbdominalCircumference() {
		CalcResult result = fb.computeAC(21*7,161.00); //21 weeks, 0 days, 200 mm
		System.out.println("mm:" + result.getValue());		
		System.out.println("AbdominalCircumference:" + result.getPercentile());	
		assertEquals(result.getPercentile(), 0.5,.01);		
	}

	@Test
	public void testWeightLater() {
		CalcResult result = fb.computeEFW(35*7,2595.0); 
		System.out.println("grams:" + result.getValue());		
		System.out.println("weight percentile:" + result.getPercentile());
		assertEquals(result.getPercentile() ,0.50, .001);

	}

	@Test
	public void testHeadCircumferenceLater() {
		CalcResult result = fb.computeHC(35*7,322.00);
		System.out.println("Head Circ mm:" + result.getValue());		
		System.out.println("HC percentile:" + result.getPercentile());	
		assertEquals(result.getPercentile(), 0.50,.001);

	}
	
	@Test
	public void testFemurLengthLater() {
		CalcResult result = fb.computeFL(35*7,68.7);
		System.out.println("mm:" + result.getValue());
		System.out.println("Femur Length percentile:" + result.getPercentile());
		assertEquals(result.getPercentile(), 0.5,.001);
		}
	
	@Test
	public void testAbdominalCircumferenceLater() {
		CalcResult result = fb.computeAC(35*7,308.00); 
		System.out.println("mm:" + result.getValue());		
		System.out.println("AbdominalCircumference:" + result.getPercentile());	
		assertEquals(result.getPercentile(),0.5, .001);		
	}


	@Test
	public void testEarSize() {
		CalcResult result = fb.computeEAR(21*7,16.00); //21 weeks, 0 days, 200 mm
		System.out.println("Ear in mm:" + result.getValue());		
		System.out.println("EAR percentile:" + result.getPercentile());	
		assertEquals(result.getPercentile() ,0.5793,.01);

	}

	@Test
	public void testBiparietal() {
		CalcResult result = fb.computeBPD(21*7,50.00); //21 weeks, 0 days, 200 mm
		System.out.println("mm:" + result.getValue());		
		System.out.println("Biparietal percentile:" + result.getPercentile());	
		assertEquals(result.getPercentile() ,0.5398,.01);
	}

	@Test
	public void testBinocularDistance() {
		CalcResult result = fb.computeBINOCULAR(21*7,35.00); //21 weeks, 0 days, 200 mm
		System.out.println("mm:" + result.getValue());		
		System.out.println("binocular Distance percentile:" + result.getPercentile());	
		assertEquals(result.getPercentile() ,0.5,.01);

	}

	@Test
	public void testTransverseCerebellarDiameter() {
		CalcResult result = fb.computeTCD(21*7,23.00); //21 weeks, 0 days, 200 mm
		System.out.println("mm:" + result.getValue());		
		System.out.println("TransverseCerebellarDiameter:" + result.getPercentile());	
		assertEquals(result.getPercentile() ,0.6443,.01);

	}
	
	@Test 
	public void TestMandible() {
		CalcResult result = fb.computeMANDIBLE(21*7,26.00); //21 weeks, 0 days, 200 mm
		System.out.println("mm:" + result.getValue());		
		System.out.println("mandible:" + result.getPercentile());	
		assertEquals(result.getPercentile() ,0.4325, .01);
		assertEquals(result.getValue(), 26.363,0.0001);
	}
	
	@Test
	public void testThoracicCirc() {
		CalcResult result = fb.computeTC(21*7,137.00); //21 weeks, 0 days, 200 mm
		System.out.println("mm:" + result.getValue());		
		System.out.println("thoracic circ:" + result.getPercentile());	
		assertEquals(result.getPercentile() ,0.5, .01);
		assertEquals(result.getValue(), 136.9488,0.0001);
		
	}
	
	@Test
	public void testClavicle() {
		CalcResult result = fb.computeCLAV(21*7,22.00); //21 weeks, 0 days, 200 mm
		System.out.println("mm:" + result.getValue());		
		System.out.println("Clav:" + result.getPercentile());	
		assertEquals(result.getPercentile() ,0.512, .01);
		
	}


	@Test
	public void testDColon() {
		CalcResult result = fb.computeCOLON(21*7,4.00); //21 weeks, 0 days, 200 mm
		System.out.println("mm:" + result.getValue());		
		System.out.println("colon diamter percentile:" + result.getPercentile());	
		assertEquals(result.getPercentile() , 0.5, .01);		
	}

	@Test
	public void testKidney() {
		CalcResult result = fb.computeKIDNEY(21*7,22.00); //21 weeks, 0 days, 200 mm
		System.out.println("mm:" + result.getValue());		
		System.out.println("kidney length percentile:" + result.getPercentile());	
		assertEquals(result.getPercentile() , 0.5239, .01);		
	}

	@Test
	public void testKidneyDiameter() {
		CalcResult result = fb.computeAPDIAM(21*7,14.00); //21 weeks, 0 days, 200 mm
		System.out.println("mm:" + result.getValue());		
		System.out.println("kidney diameter percentile:" + result.getPercentile());	
		assertEquals(result.getPercentile() , 0.484,.01);		
	}

	@Test
	public void testRenalPelvisDiameter() {
		CalcResult result = fb.computePELVIS(21*7,2.3); //21 weeks, 0 days, 200 mm
		System.out.println("mm:" + result.getValue());		
		System.out.println("Renal Pelvis percentile:" + result.getPercentile());	
		assertEquals(result.getPercentile() , 0.5, .01);		
	}
	
	
	
	@Test
	public void testUmbilicalArteryResistanceIndex21WeeksLower() {
		CalcResult result = fb.computeUARI(21*7, 0.70928);
		System.out.println("mm:" + result.getValue());
		System.out.println("UmbilicalArteryResistanceIndex (21,Lower):"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.2420, .01);
	}
	
	@Test
	public void testUmbilicalArteryResistanceIndex21WeeksHigher() {
		CalcResult result = fb.computeUARI(21*7, 0.7944);
		System.out.println("mm:" + result.getValue());
		System.out.println("UmbilicalArteryResistanceIndex (21,Higher):"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.7580, .01);
	}
	
	@Test
	public void testUmbilicalArteryResistanceIndex21WeeksSame() {
		CalcResult result = fb.computeUARI(21*7, 0.75254);
		System.out.println("mm:" + result.getValue());
		System.out.println("UmbilicalArteryResistanceIndex (21, Same):"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);
	}
	
	@Test
	public void testUmbilicalArteryResistanceIndex40WeeksLower() {
		CalcResult result = fb.computeUARI(40*7, 0.51052);
		System.out.println("mm:" + result.getValue());
		System.out.println("UmbilicalArteryResistanceIndex (40, Lower):"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.2420, .01);
	}
	
	@Test
	public void testUmbilicalArteryResistanceIndex40WeeksHigher() {
		CalcResult result = fb.computeUARI(40*7, 0.57877);
		System.out.println("mm:" + result.getValue());
		System.out.println("UmbilicalArteryResistanceIndex (40, Higher):"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.7580, .01);
	}
	
	@Test
	public void testUmbilicalArteryResistanceIndex40WeeksSame() {
		CalcResult result = fb.computeUARI(40*7, 0.55399);
		System.out.println("mm:" + result.getValue());
		System.out.println("UmbilicalArteryResistanceIndex (40, Same):"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);
	}
	
	@Test
	public void testUmbilicalArterySystolicToDiastolicRatio21WeeksLower() {
		CalcResult result = fb.computeUASD(21*7, 3.8);
		System.out.println("mm:"+result.getValue());
		System.out.println("UmbilicalArterySystolicToDiastolicRatio (21, Lower):"+ result.getPercentile());
		assertEquals(result.getPercentile() ,.4052,.001);
	}
	
	@Test
	public void testUmbilicalArterySystolicToDiastolicRatio21WeeksHigher() {
		CalcResult result = fb.computeUASD(21*7, 4.50);
		System.out.println("mm:"+result.getValue());
		System.out.println("UmbilicalArterySystolicToDiastolicRatio (21, Higher):"+ result.getPercentile());
		assertEquals(result.getPercentile() ,.67,.001);
	}
	
	@Test
	public void testUmbilicalArterySystolicToDiastolicRatio21WeeksSame() {
		CalcResult result = fb.computeUASD(21*7, 4.03);
		System.out.println("mm:"+result.getValue());
		System.out.println("UmbilicalArterySystolicToDiastolicRatio (21, Same):"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.504, .001);
	}
	
	@Test
	public void testUmbilicalArterySystolicToDiastolicRatio40WeeksLower() {
		CalcResult result = fb.computeUASD(40*7, 2.2);
		System.out.println("mm:"+result.getValue());
		System.out.println("UmbilicalArterySystolicToDiastolicRatio (40, Lower):"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.4522, .001);
	}
	
	@Test
	public void testUmbilicalArterySystolicToDiastolicRatio40WeeksHigher() {
		CalcResult result = fb.computeUASD(40*7, 2.5);
		System.out.println("mm:"+result.getValue());
		System.out.println("UmbilicalArterySystolicToDiastolicRatio (40, Higher):"+ result.getPercentile());
		assertEquals(result.getPercentile() ,.9082,.001);
	}
	
	@Test
	public void testUmbilicalArterySystolicToDiastolicRatio40WeeksSame() {
		CalcResult result = fb.computeUASD(40*7, 2.237);
		System.out.println("mm:"+result.getValue());
		System.out.println("UmbilicalArterySystolicToDiastolicRatio (40, Same):"+ result.getPercentile());
		assertEquals(result.getPercentile() ,.5,.001);
	}
	
	@Test 
	public void testTibia() {
		CalcResult result = fb.computeTIB(21*7, 30.0);	
		System.out.println("mm:" + result.getValue());
		System.out.println("tibia:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);

	}
	@Test 
	public void testFibula() {
		CalcResult result = fb.computeFIB(21*7, 30.3);	
		System.out.println("mm:" + result.getValue());
		System.out.println("fibula:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.492, .01);
		assertEquals(result.getValue(), 30.34,.001);

	}
	
	@Test 
	public void testHumerous() {
		CalcResult result = fb.computeHUM(21*7, 32.8);	
		System.out.println("mm:" + result.getValue());
		System.out.println("humerous:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);

	}
	@Test 
	public void testUlna() {
		CalcResult result = fb.computeULNA(21*7, 30.6);	
		System.out.println("mm:" + result.getValue());
		System.out.println("ulna:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);

	}
	
	@Test 
	public void testFoot() {
		CalcResult result = fb.computeFOOT(21*7, 36.0);	
		System.out.println("mm:" + result.getValue());
		System.out.println("Foot:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);

	}
	@Test 
	public void testRadius() {
		CalcResult result = fb.computeRAD(21*7, 28.0);	
		System.out.println("mm:" + result.getValue());
		System.out.println("Radius:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);

	}
	
	@Test 
	public void testDuctusVenosusPreload() {
		CalcResult result = fb.computeDVPRE(21*7, .507);	
		System.out.println("mm:" + result.getValue());
		System.out.println("DuctusVenosusPreload Index:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);
	}
	
	@Test 
	public void testDuctusVenosusPeakVelocityIndex() {
		CalcResult result = fb.computeDVPVI(21*7, .579);	
		System.out.println("mm:" + result.getValue());
		System.out.println("DuctusVenosusPeakVelocityIndex:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);
	}
	
	@Test
	public void testUmbilicalArterySD() {
		CalcResult result = fb.computeUASD(21*7, 4.025);	
		System.out.println("mm:" + result.getValue());
		System.out.println("UmbilicalArterySD:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);
	}
	
	@Test
	public void testIVCPreload() {
		CalcResult result = fb.computeIVCP(21*7, .181);	
		System.out.println("mm:" + result.getValue());
		System.out.println("IVCPreload:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);
	}
	
	@Test
	public void testIVCPulse() {
		CalcResult result = fb.computeIVCPUL(21*7, 1.768);	
		System.out.println("mm:" + result.getValue());
		System.out.println("IVCPulse:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);
	}
	@Test
	public void testUmbilicalArteryPulsatilityIndex() {
		CalcResult result = fb.computeUAPI(21*7, 1.23);	
		System.out.println("mm:" + result.getValue());
		System.out.println("UmbilicalArteryPulsatilityIndex:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.504, .01);
	}
	
	@Test
	public void testDuctusVenosusPulsatilityIndex() {
		CalcResult result = fb.computeDVPI(21*7, 0.642);	
		System.out.println("mm:" + result.getValue());
		System.out.println("DuctusVenosusPulsatilityIndex:"+ result.getPercentile());
		assertEquals(result.getPercentile() , 0.5, .01);
	}

	@Test
	public void testDuctusVenosusPulsatilityIndex2() {
		CalcResult result = fb.computeDVPI(25*7, 0.64);	
		System.out.println("mm:" + result.getValue());
		System.out.println("DuctusVenosusPulsatilityIndex:"+ result.getPercentile());
		assertEquals( 0.5,result.getPercentile() , .01);
	}

}
