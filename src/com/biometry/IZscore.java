package com.biometry;

public interface IZscore {

	public abstract double findPercentile(double zscore);

}