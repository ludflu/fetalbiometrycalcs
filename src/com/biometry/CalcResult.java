package com.biometry;

public class CalcResult {

		String msg;
		Double percentile;
		Double value;
		
		public String getMsg() {
			return msg;
		}
		public Double getPercentile() {
			return percentile;
		}
		public Double getValue() {
			return value;
		}

		public CalcResult(String msg, Double percentile, Double value) {
			this.msg = msg;
			this.percentile = percentile;
			this.value = value;
		}
}
