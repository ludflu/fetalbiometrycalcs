package com.biometry;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

//This class describes meta-data about each calculation
//and allows you to programatically execute them.
//Code is data! Data is code!
//this allows us to store references to function in a menu item,
//and invoke each one generically

//I have no idea if this will translate to ObjC
//I might need to re-write it in ObjC to use selectors instead of reflection


public class MetaCalc implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -440308282946768943L;
	String methodName;
	String desc;
	String unit;
	String url;
	String citation;
	private transient IFetalBiometry biometry;

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public IFetalBiometry getBiometry() {
		return biometry;
	}

	public void setBiometry(IFetalBiometry biometry) {
		this.biometry = biometry;
	}

	public String getUnit() {
		return unit;
	}

	public String getUrl() {
		return url;
	}

	public String getCitation() {
		return citation;
	}

	public String getMethodName() {
		return methodName;
	}
	
	public String toString() {
		if (this.desc != null) {
			return this.desc;
		} else {
			return this.methodName;
		}
	}

	public MetaCalc(IFetalBiometry b, String methodName, String unit, String url, String citation) {
		this.biometry = b;
		this.methodName = methodName;
		this.unit = unit;
		this.url = url;
		this.citation = citation;
	}
	
	public MetaCalc(IFetalBiometry b,String methodName,   String unit,String desc, String url, String citation) {
		this.biometry = b;
		this.desc = desc;
		this.methodName = methodName;
		this.unit = unit;
		this.url = url;
		this.citation = citation;
	}
	public CalcResult invoke(int days, double measurement) {
		
		try {		
			
			java.lang.reflect.Method method = biometry.getClass().getMethod(methodName, int.class, double.class);
			return (CalcResult) method.invoke(biometry,days, measurement);
			
		} catch (SecurityException e) {
			throw new BiometryException("Security Problem executing " + methodName,e);
		} catch (NoSuchMethodException e) {
			throw new BiometryException("No such method " + methodName,e);
		} catch (IllegalArgumentException e) {
			throw new BiometryException("bad argument for " + methodName,e);
		} catch (IllegalAccessException e) {
			throw new BiometryException("bad access for " + methodName,e);
		} catch (InvocationTargetException e) {
			throw new BiometryException("bad target for " + methodName,e);
		}
	}
	
	
	public static List<MetaCalc> getCalcs(IFetalBiometry bio) {
		List<MetaCalc> calcs = new ArrayList<MetaCalc>();
		calcs.add(new MetaCalc(bio,"computeEFW","grams","Weight","http://www.ncbi.nlm.nih.gov/pubmed/1887021?","ln weight (g)=0.578 + 0.332*MA -.00354*MA^2 \nHadlock FP, et al., \nIn utero analysis of fetal growth: a sonographic weight standard.\nRadiology. 1991 Oct;181(1):129-33."));
		calcs.add(new MetaCalc(bio,"computeHC","mm","Head Circumference","http://www.ncbi.nlm.nih.gov/pubmed/6739822?","HC= -11.48 +1.56 * MA -.0002584*MA^3.SD =1 cm\nHadlock FP, et al., Estimating fetal age:Computer-assisted analysis of multiple fetal growth parameters. Radiology 1984; 152:497-5011."));
		calcs.add(new MetaCalc(bio,"computeAC","mm","Abdominal Circumference","http://www.ncbi.nlm.nih.gov/pubmed/6739822?","AC = -13.3 + 1.61 * (MA) - 0.00998* MA^2. SD= 13.4 mm \nHadlock FP, et al., Estimating fetal age:Computer-assisted analysis of multiple fetal growth parameters. Radiology 1984; 152:497-5011."));
		calcs.add(new MetaCalc(bio,"computeFL","mm","Femur Length","http://www.ncbi.nlm.nih.gov/pubmed/6739822?","FL = -3.91 + 0.427 *(MA) - 0.0034* MA^2. SD= 3 mm \nHadlock FP, et al., Estimating fetal age:Computer-assisted analysis of multiple fetal growth parameters. Radiology 1984; 152:497-5011."));
		calcs.add(new MetaCalc(bio,"computeBPD","mm","Biparietal Diameter","http://www.ncbi.nlm.nih.gov/pubmed/6739822?","BPD = -3.08 + 0.41*MA - 0.000061*MA^3. SD= 3mm \nHadlock FP, et al., Estimating fetal age:Computer-assisted analysis of multiple fetal growth parameters. Radiology 1984; 152:497-5011."));
		calcs.add(new MetaCalc(bio,"computeUARI","index","Umbilical artery resistance index","http://www.ncbi.nlm.nih.gov/pubmed/1590875","UmbA RI = 0.97199-0.01045*gestational age. \nKofinas AD, et al.Uteroplacental Doppler flow velocity waveform indices in normal pregnancy: a statistical exercise and the development of appropriate reference values. Am J Perinatol. 1992 Mar;9(2):94-101"));
		calcs.add(new MetaCalc(bio,"computeUASD","index","Umbilical artery S/D ","http://www.ncbi.nlm.nih.gov/pubmed/1590875","UmbA SD = 1/(1-(0.971-0.01045*gestational age)).  \nKofinas AD, et al.Uteroplacental Doppler flow velocity waveform indices in normal pregnancy: a statistical exercise and the development of appropriate reference values. Am J Perinatol. 1992 Mar;9(2):94-101"));
		calcs.add(new MetaCalc(bio,"computeHUM","mm","Humerus length","http://www.ncbi.nlm.nih.gov/pubmed/6335042","Humerus = -16.24 + 0.76315*MA + 0.1683*MA^2 -0.0056212* MA^3 +0.000055666*MA^4  SD= (0.0147*MA + 2.2015) \nJeanty P , et. al., A longitudinal study of fetal limb growth. Am J Perinatol. 1984 Jan;1(2):136-44. "));
		calcs.add(new MetaCalc(bio,"computeULNA","mm","Ulna length","http://www.ncbi.nlm.nih.gov/pubmed/6335042","Ulna = -34.313 + 3.8685*MA - 0.036949*MA^2  SD= (0.0147*MA + 2.2015) \nJeanty P , et. al., A longitudinal study of fetal limb growth. Am J Perinatol. 1984 Jan;1(2):136-44."));
		calcs.add(new MetaCalc(bio,"computeRAD","mm","Radius","http://www.ncbi.nlm.nih.gov/pubmed/12797037?","Radius length = -29.090 + 3.37*GA 0.031*GA^2. \nExacoustos C, Ultrasound measurements of fetal limb bones. Ultrasound Obstet Gynecol. 1991 Sep 1;1(5):325-30."));
		calcs.add(new MetaCalc(bio,"computeTIB","mm","Tibia","http://www.ncbi.nlm.nih.gov/pubmed/6335042","Tibia = -5.555 - 0.915554*MA + 0.23359*MA^2 -0.00638* MA^3 +0.000055801*MA^4  SD= (0.0152*MA + 2.209) \nJeanty P , et. al., A longitudinal study of fetal limb growth. Am J Perinatol. 1984 Jan;1(2):136-44."));
		calcs.add(new MetaCalc(bio,"computeFIB","mm","Fibula","http://www.ncbi.nlm.nih.gov/pubmed/12797037?","Fibula length= -36.563 +3.963 X GA -0.037 X GA^2  \nExacoustos C, Ultrasound measurements of fetal limb bones. Ultrasound Obstet Gynecol. 1991 Sep 1;1(5):325-30."));
		calcs.add(new MetaCalc(bio,"computeTC","mm","Thoracic circumference","http://www.ncbi.nlm.nih.gov/pubmed/3555087?","Chitkara, U., Rosenberg J., Chervenak, FA et al ,Prenatal sonographic assessment of the fetal thorax:normal values. Am J Obstet Gynecol 1987; 156,1069-74 "));
		calcs.add(new MetaCalc(bio,"computeFOOT","mm","Foot","http://www.ncbi.nlm.nih.gov/pubmed/3548369?","Foot length= -36.74 + 4.045 X GA -0.02728 X GA ^2. (range +/- 11%)  \nMercer BM , et. al., Fetal foot length as a predictor of gestational age.Am J Obstet Gynecol. 1987 Feb;156(2):350-5."));
		calcs.add(new MetaCalc(bio,"computeBINOCULAR","mm","Binocular Distance","http://www.ncbi.nlm.nih.gov/pubmed/6803295?","Binocular distance= -20.085293 + 3.3446277 X GA -0.034041701 X GA ^2. \nJeanty P, et al.,  Fetal ocular biometry by ultrasound. Radiology. 1982 May;143(2):513-6."));
		calcs.add(new MetaCalc(bio,"computeTCD","mm","Transverse Cerebellar Diameter","http://www.ncbi.nlm.nih.gov/pubmed/10722905?","Transverse cerebellar diameter=-10.632 + 1.5486 X GA ; SD= 2.96 \nChang CH ,et al., Three-dimensional ultrasound in the assessment of fetal cerebellar transverse and antero-posterior diameters.Ultrasound Med Biol. 2000 Feb;26(2):175-82."));
		calcs.add(new MetaCalc(bio,"computeMANDIBLE","mm","Mandible Length","http://www.ncbi.nlm.nih.gov/pubmed/12797096?","Mandible length= -20.41+ 2.97 X GA -0.027 X GA ^2.  SD= 2.077 \nOtto C, Platt LD.The fetal mandible measurement: an objective determination of fetal jaw size.Ultrasound Obstet Gynecol. 1991 Jan 1;1(1):12-7."));
		calcs.add(new MetaCalc(bio,"computeCLAV","mm","Clavicle Length ","http://www.ncbi.nlm.nih.gov/pubmed/3903198?","Clavicle length= 1.118303 + 0.988639 X GA . SD = 2.92. \nYarkoni S, Clavicular measurement: a new biometric parameter for fetal evaluation.J Ultrasound Med. 1985 Sep;4(9):467-70."));
		calcs.add(new MetaCalc(bio,"computeIVCP","(a/S)","Inferior Vena Cava Preload index","http://www.ncbi.nlm.nih.gov/pubmed/14689526?","IVC Preload Index= .1788 + 0.0001 X GA . SD = 0.1. \nBaschat AA.Relationship between placental blood flow resistance and precordial venous Doppler indices.Ultrasound Obstet Gynecol. 2003 Dec;22(6):561-6."));
		calcs.add(new MetaCalc(bio,"computeIVCPUL","(S-a)/Tamx","Inferior Vena Cava Pulsatility index","http://www.ncbi.nlm.nih.gov/pubmed/14689526?","IVC Pulsatility Index= 1.8223 -.0026 X GA . SD = 0.42. \nBaschat AA.Relationship between placental blood flow resistance and precordial venous Doppler indices.Ultrasound Obstet Gynecol. 2003 Dec;22(6):561-6."));
		calcs.add(new MetaCalc(bio,"computeCOLON","mm","Internal Colon Diameter","http://www.ncbi.nlm.nih.gov/pubmed/3300224?","Harris RD, et al. Anorectal atresia: prenatal sonographic diagnosis.AJR Am J Roentgenol. 1987 Aug;149(2):395-400."));
		calcs.add(new MetaCalc(bio,"computeUAPI","index","Umbilical Artery Pulsatility Index","http://www.ncbi.nlm.nih.gov/pubmed/15883983?","Acharya G, Reference ranges for serial measurements of blood velocity and pulsatility index at the intra-abdominal portion, and fetal and placental ends of the umbilical artery.Ultrasound Obstet Gynecol. 2005 Aug;26(2):162-9."));
		calcs.add(new MetaCalc(bio,"computeDVPI","(S-a)/Tamx","Ductus Venosus Pulsatility Index ","http://www.ncbi.nlm.nih.gov/pubmed/14689526?","Ductus venosus Pulsatility Index= 0.6625 -.001 X GA . SD = 0.14. \nBaschat AA.Relationship between placental blood flow resistance and precordial venous Doppler indices.Ultrasound Obstet Gynecol. 2003 Dec;22(6):561-6."));
		calcs.add(new MetaCalc(bio,"computeDVPRE","(a/S)","Ductus Venosus Preload Index","http://www.ncbi.nlm.nih.gov/pubmed/14689526?","Ductus venosus preload index= 0.5197 -.0006 X GA . SD = 0.1  \nBaschat AA.Relationship between placental blood flow resistance and precordial venous Doppler indices.Ultrasound Obstet Gynecol. 2003 Dec;22(6):561-6."));
		calcs.add(new MetaCalc(bio,"computeDVPVI","(S-a)/D","Ductus Venosus peak velocity index","http://www.ncbi.nlm.nih.gov/pubmed/14689526?","Ductus venosus peak velocity index= 0.592 -.0006 X GA . SD = 0.12  \nBaschat AA.Relationship between placental blood flow resistance and precordial venous Doppler indices.Ultrasound Obstet Gynecol. 2003 Dec;22(6):561-6."));
		calcs.add(new MetaCalc(bio,"computeKIDNEY","mm","Kidney Length","http://www.ncbi.nlm.nih.gov/pubmed/14634973?","Kidney length = exp(5.4141 * log(w/10)-5.50562*SQRT(w/10) +6.3939) \nChitty LS, Altman DG.Charts of fetal size: kidney and renal pelvis measurements.Prenat Diagn. 2003 Nov;23(11):891-7."));
		calcs.add(new MetaCalc(bio,"computePELVIS","mm","Renal Pelvis antero-posterior diameter","http://www.ncbi.nlm.nih.gov/pubmed/14634973?","Renal pelvis antero-posterior diameter= 0.098592 X w + 0.19718 \nChitty LS, Altman DG.Charts of fetal size: kidney and renal pelvis measurements.Prenat Diagn. 2003 Nov;23(11):891-7."));
		calcs.add(new MetaCalc(bio,"computeAPDIAM","mm","Kidney antero-posterior diameter","http://www.ncbi.nlm.nih.gov/pubmed/14634973?","Kidney antero-posterior diameter= exp(15.283 X log(w/10)-14.301 X Sqrt(w/10) + 17.055) \nChitty LS, Altman DG.Charts of fetal size: kidney and renal pelvis measurements.Prenat Diagn. 2003 Nov;23(11):891-7."));
		return calcs;
	}
}
