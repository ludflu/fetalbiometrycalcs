package com.biometry;

import java.util.ArrayList;
import java.util.List;

import com.biometry.AgeStat.BioParam;

//
//implements the age estimation method described in
// Estimating fetal age: computer-assisted analysis of multiple fetal growth parameters.
// Radiology. 1984 Aug;152(2):497-501.
// Hadlock FP, Deter RL, Harrist RB, Park SK.
//
//http://www.ncbi.nlm.nih.gov/pubmed/6739822

public class AgeFinder {
	final List<AgeStat> stats = AgeStat.getStats();

	public double findAge(double val, BioParam p) {

		double diff = 100.0;
		double found = -1000.0;

		for (AgeStat a : stats) {
			double t = 0.0;

			switch (p) {
			case BPD:
				t = a.getBpd();
				break;
			case HC:
				t = a.getHc();
				break;

			case AC:
				t = a.getAc();
				break;

			case FL:
				t = a.getFl();
				break;
			}
			double d = Math.abs(t - val);
			if (d < diff) {
				diff = d;
				found = a.getAge();
			}

		}
		return found;
	}
	
	
	private boolean isNone(Double d) {
		return (d == null) || (d==0.0);
	}
	
	public double estimateAge(Double bpd, Double hc, Double ac, Double fl) {

		List<Double> est = new ArrayList<Double>();
		
		if (!isNone(bpd)) {
			est.add( findAge(bpd,BioParam.BPD));
		}
		if  (!isNone(hc)) { 
			est.add( findAge(hc,BioParam.HC));			
		}
		if  (!isNone( ac)) { 
			est.add( findAge(ac,BioParam.AC));
		}
		if  (!isNone(fl)) { 
			est.add( findAge(fl,BioParam.FL));
		}

		double sum = 0.0;
		for (double v : est) {
			sum += v;
		}
		
		return sum / est.size();
	}

}
