package com.biometry;

public interface IFetalBiometry {

	public abstract CalcResult performCalc(int days, double value, MetaCalc calc);

	//predicted weight
	public abstract CalcResult computeEFW(int days, double measured);

	//head circumference
	public abstract CalcResult computeHC(int days, double size);

	//ear length
	public abstract CalcResult computeEAR(int days, double size);

	// predicted mean biparietal
	public abstract CalcResult computeBPD(int days, double size);

	//predicted mean abdominal circumference
	public abstract CalcResult computeAC(int days, double size);

	//femur length
	public abstract CalcResult computeFL(int days, double size);

	// umbilical artery resistance index
	public abstract CalcResult computeUARI(int days, double size);

	// umbilical artery S/D 
	public abstract CalcResult computeUASD(int days, double size);

	//humerus length 
	public abstract CalcResult computeHUM(int days, double size);

	//ulna length
	public abstract CalcResult computeULNA(int days, double size);

	//radius length 
	public abstract CalcResult computeRAD(int days, double size);

	//tibia length
	public abstract CalcResult computeTIB(int days, double size);

	//fibula length
	public abstract CalcResult computeFIB(int days, double size);

	// thoracic circumference
	public abstract CalcResult computeTC(int days, double size);

	//foot length
	public abstract CalcResult computeFOOT(int days, double size);

	//binocular distance
	public abstract CalcResult computeBINOCULAR(int days, double size);

	//transverse cerebellar diameter
	public abstract CalcResult computeTCD(int days, double size);

	//mandible length
	public abstract CalcResult computeMANDIBLE(int days, double size);

	//clavicle length 
	public abstract CalcResult computeCLAV(int days, double size);

	//inferior vena cava preload index 
	public abstract CalcResult computeIVCP(int days, double size);

	//inferior vena cava pulsatility index (S-a){}/Tamx
	public abstract CalcResult computeIVCPUL(int days, double size);

	//mean internal colon diameter
	public abstract CalcResult computeCOLON(int days, double size);

	// umbilical artery pulsatility index
	public abstract CalcResult computeUAPI(int days, double size);

	// ductus venosus pulsatility index (S-a){}/Tamx
	public abstract CalcResult computeDVPI(int days, double size);

	//ductus venosus preload index (a/S){}
	public abstract CalcResult computeDVPRE(int days, double size);

	//ductus venosus peak velocity index (S-a){}/D 
	public abstract CalcResult computeDVPVI(int days, double size);

	//Kidney length 
	public abstract CalcResult computeKIDNEY(int days, double size);

	//mean renal pelvis antero-posterior diameter
	public abstract CalcResult computePELVIS(int days, double size);

	//mean kidney antero-posterior diameter
	public abstract CalcResult computeAPDIAM(int days, double size);


}