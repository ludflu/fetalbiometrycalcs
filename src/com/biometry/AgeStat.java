package com.biometry;

import java.util.ArrayList;
import java.util.List;

public class AgeStat {

	
	public enum BioParam {BPD,HC,AC,FL};
	
	private Double age;
	private Double bpd;
	private Double hc;
	private Double ac;
	private Double fl;

	public Double getAge() {
		return age;
	}

	public Double getBpd() {
		return bpd;
	}

	public Double getHc() {
		return hc;
	}

	public Double getAc() {
		return ac;
	}

	public Double getFl() {
		return fl;
	}

	public AgeStat(double age, double bpd, double hc, double ac, double fl) {
		this.age = age;
		this.bpd = bpd;
		this.hc = hc;
		this.ac = ac;
		this.fl = fl;

	}

	public AgeStat(Double age, Double bpd, Double hc, Double ac, Double fl) {

		this.age = age;
		this.bpd = bpd;
		this.hc = hc;
		this.ac = ac;
		this.fl = fl;
	}

	public static List<AgeStat> getStats() {
		List<AgeStat> stats = new ArrayList<AgeStat>();

		stats.add(new AgeStat(12.0, 1.7, 6.8, 4.6, 0.7));
		stats.add(new AgeStat(12.5, 1.9, 7.5, 5.3, 0.9));
		stats.add(new AgeStat(13.0, 2.1, 8.2, 6, 1.1));
		stats.add(new AgeStat(13.5, 2.3, 8.9, 6.7, 1.2));
		stats.add(new AgeStat(14, 2.5, 9.7, 7.3, 1.4));
		stats.add(new AgeStat(14.5, 2.7, 10.4, 8, 1.6));
		stats.add(new AgeStat(15, 2.9, 11, 8.6, 1.7));
		stats.add(new AgeStat(15.5, 3.1, 11.7, 9.3, 1.9));
		stats.add(new AgeStat(16, 3.2, 12.4, 9.9, 2));
		stats.add(new AgeStat(16.5, 3.4, 13.1, 10.6, 2.2));
		stats.add(new AgeStat(17, 3.6, 13.8, 11.2, 2.4));
		stats.add(new AgeStat(17.5, 3.8, 14.4, 11.9, 2.5));
		stats.add(new AgeStat(18, 3.9, 15.1, 12.5, 2.7));
		stats.add(new AgeStat(18.5, 4.1, 15.8, 13.1, 2.8));
		stats.add(new AgeStat(19, 4.3, 16.4, 13.7, 3));
		stats.add(new AgeStat(19.5, 4.5, 17, 14.4, 3.1));
		stats.add(new AgeStat(20, 4.6, 17.7, 15, 3.3));
		stats.add(new AgeStat(20.5, 4.8, 18.3, 15.6, 3.4));
		stats.add(new AgeStat(21, 5, 18.9, 16.2, 3.5));
		stats.add(new AgeStat(21.5, 5.1, 19.5, 16.8, 3.7));
		stats.add(new AgeStat(22, 5.3, 20.1, 17.4, 3.8));
		stats.add(new AgeStat(22.5, 5.5, 20.7, 17.9, 4));
		stats.add(new AgeStat(23, 5.6, 21.3, 18.5, 4.1));
		stats.add(new AgeStat(23.5, 5.8, 21.9, 19.1, 4.2));
		stats.add(new AgeStat(24, 5.9, 22.4, 19.7, 4.4));
		stats.add(new AgeStat(24.5, 6.1, 23, 20.2, 4.5));
		stats.add(new AgeStat(25, 6.2, 23.5, 20.8, 4.6));
		stats.add(new AgeStat(25.5, 6.4, 24.1, 21.3, 4.7));
		stats.add(new AgeStat(26, 6.5, 24.6, 21.9, 4.9));
		stats.add(new AgeStat(26.5, 6.7, 25.1, 22.4, 5));
		stats.add(new AgeStat(27, 6.8, 25.6, 23, 5.1));
		stats.add(new AgeStat(27.5, 6.9, 26.1, 23.5, 5.2));
		stats.add(new AgeStat(28, 7.1, 26.6, 24, 5.4));
		stats.add(new AgeStat(28.5, 7.2, 27.1, 24.6, 5.5));
		stats.add(new AgeStat(29, 7.3, 27.5, 25.1, 5.6));
		stats.add(new AgeStat(29.5, 7.5, 28, 25.6, 5.7));
		stats.add(new AgeStat(30, 7.6, 28.4, 26.1, 5.8));
		stats.add(new AgeStat(30.5, 7.7, 28.8, 26.6, 5.9));
		stats.add(new AgeStat(31, 7.8, 29.3, 27.1, 6));
		stats.add(new AgeStat(31.5, 7.9, 29.7, 27.6, 6.1));
		stats.add(new AgeStat(32, 8.1, 30.1, 28.1, 6.2));
		stats.add(new AgeStat(32.5, 8.2, 30.4, 28.6, 6.3));
		stats.add(new AgeStat(33, 8.3, 30.8, 29.1, 6.4));
		stats.add(new AgeStat(33.5, 8.4, 31.2, 29.5, 6.5));
		stats.add(new AgeStat(34, 8.5, 31.5, 30, 6.6));
		stats.add(new AgeStat(34.5, 8.6, 31.8, 30.5, 6.7));
		stats.add(new AgeStat(35, 8.7, 32.2, 30.9, 6.8));
		stats.add(new AgeStat(35.5, 8.8, 32.5, 31.4, 6.9));
		stats.add(new AgeStat(36, 8.9, 32.8, 31.8, 7));
		stats.add(new AgeStat(36.5, 8.9, 33, 32.3, 7.1));
		stats.add(new AgeStat(37, 9, 33.3, 32.7, 7.2));
		stats.add(new AgeStat(37.5, 9.1, 33.5, 33.2, 7.3));
		stats.add(new AgeStat(38, 9.2, 33.8, 33.6, 7.4));
		stats.add(new AgeStat(38.5, 9.2, 34, 34, 7.4));
		stats.add(new AgeStat(39, 9.3, 34.2, 34.4, 7.5));
		stats.add(new AgeStat(39.5, 9.4, 34.4, 34.8, 7.6));
		stats.add(new AgeStat(40, 9.4, 34.6, 35.3, 7.7));

		return stats;

	}
	

}
