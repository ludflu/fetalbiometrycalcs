package com.biometry;

public class BiometryException extends RuntimeException {

	private static final long serialVersionUID = -7038079897824792715L;

	public BiometryException(String msg, Exception e) {
		super(msg, e);
	}
}
