package com.biometry;

import java.lang.reflect.InvocationTargetException;

import com.biometry.CalcResult;

public class FetalBiometry implements IFetalBiometry {

	static IZscore percentile = new Percentile();
	static final double DAY_ADJ = 0.142857;

	private double roundNumber(double num, double dec) {
		return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
	}

	private double adjustDays(int days) {
		return DAY_ADJ * days;
	}

	private double findPercentile(double zscore) {
		return percentile.findPercentile(zscore);
	}

	// predicted weight
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeEFWH(int, double)
	 */
	@Override
	public CalcResult computeEFW(int days, double measured) {
		double weeks = days / 7;
		double predictedWeight = (Math.exp(0.578 + (.332 * weeks)
				- (.00354 * Math.pow(weeks, 2))));
		double diff = measured - predictedWeight;
		double sd = 0.13 * predictedWeight;
		double zscore = diff / sd;
		return new CalcResult("Predicted weight in grams",
				findPercentile(zscore), predictedWeight);
	}

	// head circumference
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeHCH(int, double)
	 */
	@Override
	public CalcResult computeHC(int days, double measured) {
		double stg = adjustDays(days);
		double predictedHeadCirc = roundNumber(-11.48 + (1.56 * stg) - (.0002548
				* Math.pow(stg, 3)),1);
		double diff = measured - predictedHeadCirc * 10.0;
		double zscore = diff / 10.0;
		return new CalcResult("Predicted head circumference in mm",
				findPercentile(zscore), predictedHeadCirc * 10.0);
	}


	// ear length
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeEAR(int, double)
	 */
	@Override
	public CalcResult computeEAR(int days, double measured) {
		double stg = adjustDays(days);
		double predicted = -10.727 + (1.25528 * stg);
		double diff = measured - predicted;

		double sd = 0.0;
		if (diff > 0) {
			double HISD1 = -0.0095 * Math.pow(stg, 2) + 1.7408 * stg - 14.39;
			sd = (HISD1 - predicted) / 1.282;
		} else if (diff < 0) {
			double LOSD1 = 0.016 * Math.pow(stg, 2) + 0.4885 * stg - 3.961;
			sd = (predicted - LOSD1) / 1.282;
		} else {
			throw new RuntimeException("freak out!");
		}
		double zscore = diff / sd;
		double percentile = findPercentile(zscore);
		return new CalcResult("Ear", percentile, predicted);
	}

	

	// predicted mean abdominal circumference
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeACH(int, double)
	 */
	@Override
	public CalcResult computeAC(int days, double measured) {

		double stg = adjustDays(days);
		double predicted = roundNumber(
				(-13.3 + 1.61 * stg - .00998 * Math.pow(stg, 2)), 1);
		double diff = (measured / 10) - predicted;
		double zscore = diff / 1.34;
		return new CalcResult("abdominal circumference in mm",
				findPercentile(zscore), predicted * 10);
	}

	// femur length
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeFLH(int, double)
	 */
	@Override
	public CalcResult computeFL(int days, double measured) {

		double stg = adjustDays(days);
		double predicted = roundNumber(
				(-3.91 + (0.427 * stg) - (0.0034 * Math.pow(stg, 2))), 2);
		double diff = (measured / 10) - predicted;
		double zscore = diff / 0.3;
		return new CalcResult("Femur Length in mm", findPercentile(zscore),
				predicted * 10);
	}

	
	// predicted mean biparietal
		/*
		 * (non-Javadoc)
		 * 
		 * @see com.biometry.IFetalBiometry#computeBPDH(int, double)
		 */
		@Override
		public CalcResult computeBPD(int days, double measured) {
			double stg = adjustDays(days);
			double stg2 = -3.08 + 0.41 * stg - .000061 * Math.pow(stg, 3);
			double predicted = roundNumber(stg2, 2);
			double diff = (measured / 10) - predicted;
			double zscore = diff / 0.3;
			return new CalcResult("biparietal in mm", findPercentile(zscore),
					predicted * 10);
		}
	
	// umbilical artery resistance index
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeUARI(int, double)
	 */
	@Override
	public CalcResult computeUARI(int days, double measured) {

		double stg = adjustDays(days);
		double predicted = 0.97199 - (0.01045 * stg);
		double diff = (measured - predicted);
		double sd = 0.06;
		double losd;
		double hisd;

		if (stg < 38) {
			losd = -0.0003 * stg + 0.0681;
			hisd = 0.0003 * stg + 0.0535;
		} else {
			losd = -0.0003 * stg + 0.0741;
			hisd = -0.0003 * stg + 0.0474;
		}

		if (diff > 0) {
			sd = hisd;
		} else {
			sd = losd;
		}

		double zscore = diff / sd;
		return new CalcResult("umbilical artery resistance index", findPercentile(zscore),
				roundNumber(predicted, 2));
	}

	// umbilical artery S/D
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeUASD(int, double)
	 */
	@Override
	public CalcResult computeUASD(int days, double measured) {
		double stg = adjustDays(days);

		double predicted = 1 / (1 - (0.971 - 0.01045 * stg));

		double PredictedRI = 1 - 1 * (1 / predicted);
		double MeasuredRI = 1 - 1 * (1 / measured);

		// get zscore
		double diff = MeasuredRI - PredictedRI;

		double sd = .06;
		double losd = 0.0;
		double hisd = 0.0;

		if (stg < 38) {
			losd = -0.0003 * stg + 0.0681;
		}
		if (stg >= 38) {
			losd = -0.0003 * stg + 0.0741;
		}
		if (stg < 38) {
			hisd = 0.0003 * stg + 0.0535;
		}
		if (stg >= 38) {
			hisd = -0.0003 * stg + 0.0474;
		}

		if (diff > 0) {
			sd = hisd;
		} else if (diff < 0) {
			sd = losd;
		} else {
			throw new RuntimeException("freak out!");
		}

		double zscore = roundNumber((diff / sd), 2);
		return new CalcResult("umbilical artery S/D", findPercentile(zscore),
				roundNumber(predicted, 2));

	}

	// humerus length
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeHUM(int, double)
	 */
	@Override
	public CalcResult computeHUM(int days, double measured) {

		double stg = adjustDays(days);

		double predicted = roundNumber(
				(-16.24 + 0.76315 * stg + 0.1683 * Math.pow(stg, 2) - 0.0056212
						* Math.pow(stg, 3) + 0.000055666 * Math.pow(stg, 4)), 1);

		double diff = measured - predicted;
		double sd = roundNumber((0.0153 * stg + 2.0948), 1);
		double zscore = diff / sd;
		return new CalcResult(" humerous", findPercentile(zscore), predicted);
	}

	// ulna length
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeULNA(int, double)
	 */
	@Override
	public CalcResult computeULNA(int days, double measured) {

		double stg = adjustDays(days);
		double predicted = roundNumber(
				(-34.313 + 3.8685 * stg - 0.036949 * Math.pow(stg, 2)), 1);
		double diff = measured - predicted;
		double sd = roundNumber((0.0147 * stg + 2.2015), 1);
		double zscore = diff / sd;
		return new CalcResult("ulna", findPercentile(zscore), predicted);

	}

	// radius length
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeRAD(int, double)
	 */
	@Override
	public CalcResult computeRAD(int days, double measured) {

		double stg = adjustDays(days);
		double predicted = roundNumber(
				((-29.09 + 3.371 * stg - 0.031 * Math.pow(stg, 2))), 1);
		double diff = measured - predicted;
		double sd = (2.1811 + 0.0156 * stg);
		double zscore = roundNumber((diff / sd), 2);
		return new CalcResult("radius", findPercentile(zscore), predicted);
	}

	// tibia length
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeTIB(int, double)
	 */
	@Override
	public CalcResult computeTIB(int days, double measured) {
		double stg = adjustDays(days);
		double predicted = roundNumber(
				(-5.555 - 0.915554 * stg + 0.23359 * Math.pow(stg, 2) - 0.00638
						* Math.pow(stg, 3) + 0.000055801 * Math.pow(stg, 4)), 1);

		double diff = measured - predicted;
		double sd = roundNumber((0.0152 * stg + 2.209), 1);
		double zscore = diff / sd;
		return new CalcResult("tibia Length in mm", findPercentile(zscore),
				roundNumber(predicted, 2));
	}

	// fibula length
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeFIB(int, double)
	 */
	@Override
	public CalcResult computeFIB(int days, double measured) {
		double stg = adjustDays(days);
		double predicted = ((-36.563 + 3.963 * stg - 0.037 * Math.pow(stg, 2)));
		double diff = measured - predicted;
		double sd = 2.209 + 0.0152 * stg;
		double zscore = roundNumber((diff / sd), 2);
		return new CalcResult("fibula Length in mm", findPercentile(zscore),
				roundNumber(predicted, 2));
	}

	// thoracic circumference
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeTC(int, double)
	 */
	@Override
	public CalcResult computeTC(int days, double measured) {
		double stg = adjustDays(days);
		double predicted = (-5.3563 + 0.9072 * stg);
		double diff = (measured / 10) - predicted;
		double zscore = roundNumber((diff / 1.66), 2);
		return new CalcResult("thoracic circumference", findPercentile(zscore),
				predicted * 10);

	}

	// foot length
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeFOOT(int, double)
	 */
	@Override
	public CalcResult computeFOOT(int days, double measured) {
		double stg = adjustDays(days);

		double predicted = roundNumber(
				(-36.74 + 4.045 * stg - 0.02728 * Math.pow(stg, 2)), 0);

		double diff = measured - predicted;
		double sd = predicted * .055;

		double zscore = roundNumber(diff / sd, 2);
		return new CalcResult("foot length", findPercentile(zscore), predicted);

	}

	// binocular distance
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeBINOCULAR(int, double)
	 */
	@Override
	public CalcResult computeBINOCULAR(int days, double measured) {
		double stg = adjustDays(days);
		double predicted = roundNumber(
				(-20.085293 + 3.3446277 * stg - 1 * 0.034041701 * Math.pow(stg,
						2)), 0);
		double diff = measured - predicted;
		double zscore = diff / 4.6;
		return new CalcResult("Binocular distance", findPercentile(zscore),
				predicted);
	}

	// transverse cerebellar diameter
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeTCD(int, double)
	 */
	@Override
	public CalcResult computeTCD(int days, double measured) {
		double stg = adjustDays(days);
		double predicted = roundNumber((-10.632 + 1.5486 * stg), 1);
		double zscore = (measured - predicted) / 2.9622;
		return new CalcResult("transverse cerebellar diameter",
				findPercentile(zscore), predicted);
	}

	// mandible length
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeMANDIBLE(int, double)
	 */
	@Override
	public CalcResult computeMANDIBLE(int days, double measured) {

		double stg = adjustDays(days);
		double predicted = roundNumber(
				(-2.41 + .297 * stg - 1 * 0.0027 * Math.pow(stg, 2)), 4);
		double diff = (measured / 10) - predicted;
		double zscore = roundNumber(diff / 0.208, 2);
		return new CalcResult("mandible length", findPercentile(zscore), predicted*10);

	}

	// clavicle length
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeCLAV(int, double)
	 */
	@Override
	public CalcResult computeCLAV(int days, double measured) {
		double stg = adjustDays(days);
		double predicted = roundNumber((1.118303 + 0.988639 * stg), 1);
		double diff = measured - predicted;
		double zscore = roundNumber(diff / 2.92, 2);
		return new CalcResult("Clavicle Length", findPercentile(zscore),
				predicted);
	}

	// inferior vena cava preload index
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeIVCPreload(int, double)
	 */
	@Override
	public CalcResult computeIVCP(int days, double measured) {
		double stg = adjustDays(days);
		double predicted = roundNumber((0.1788 + 0.0001 * stg), 3);
		double diff = measured - predicted;
		double sd = 0.1;
		double zscore = roundNumber((diff / sd), 2);
		return new CalcResult("inferior vena cava preload index",
				findPercentile(zscore), predicted);
	}

	// inferior vena cava pulsatility index (S-a){}/Tamx
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeIVCPuls(int, double)
	 */
	@Override
	public CalcResult computeIVCPUL(int days, double measured) {
		double stg = adjustDays(days);
		double predicted = roundNumber((1.8223 - 0.0026 * stg), 4);
		double diff = measured - predicted;
		double sd = 0.42;
		double zscore = roundNumber((diff / sd), 2);
		return new CalcResult("inferior vena cava pulsatility index",
				findPercentile(zscore), predicted);

	}

	// mean internal colon diameter
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeDCOLON(int, double)
	 */
	@Override
	public CalcResult computeCOLON(int days, double measured) {
		double stg = adjustDays(days);
		double predicted = roundNumber(
				(+7.6 - 0.5936 * stg + 0.0199 * Math.pow(stg, 2)), 0);
		double diff = measured - predicted;

		double SD = 1;
		if (stg > 24 && stg < 28 && diff > 0) {
			SD = 1.5;
		} else if (stg >= 28 && stg < 36) {
			SD = 1.5;
		} else if (stg >= 36 && stg < 38) {
			SD = 2;
		} else if (stg >= 38 && stg < 40 && diff < 0) {
			SD = 2.5;
		} else if (stg >= 38 && stg < 40 && diff > 0) {
			SD = 2;
		} else if (stg >= 40 && diff < 0) {
			SD = 3;
		} else if (stg >= 40 && diff > 0) {
			SD = 2;
		}

		double zscore = roundNumber((diff / SD), 2);
		return new CalcResult("internal colon diameter", findPercentile(zscore),
				predicted);

	}

	// umbilical artery pulsatility index
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeUAPI(int, double)
	 */
	@Override
	public CalcResult computeUAPI(int days, double measured) {

		double stg = adjustDays(days);
		double predicted = Math.exp(1.5075 - 0.2843 * Math.pow(stg, 0.5));
		double diff = measured - predicted;
		double hisd = ((9.5428 * Math.pow(stg, -.5932) - predicted)) / 1.6542;
		double low = roundNumber(1.8862 * Math.pow(2.7182818, (-0.0327 * stg)),
				2);
		double losd = (predicted - low) / 1.6542;
		double sd = 0.0;

		if (diff > 0) {
			sd = hisd;
		} else if (diff < 0) {
			sd = losd;
		} else {
			throw new RuntimeException("freak out!");
		}

		double zscore = roundNumber((diff / sd), 2);
		return new CalcResult("umbilical artery pulsatility index",
				findPercentile(zscore), predicted);

	}

	// ductus venosus pulsatility index (S-a){}/Tamx
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeDVPI(int, double)
	 */
	@Override
	public CalcResult computeDVPI(int days, double measured) {
		double stg = adjustDays(days);
		double predicted = roundNumber((0.6625 - 0.001 * stg), 4);
		double diff = measured - predicted;
		double sd = 0.14;
		double zscore = roundNumber((diff / sd), 2);
		return new CalcResult("ductus venosus pulsatility index",
				findPercentile(zscore), predicted);
	}

	// ductus venosus preload index (a/S){}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeDVPre(int, double)
	 */
	@Override
	public CalcResult computeDVPRE(int days, double measured) {
		double stg = adjustDays(days);

		double predicted = roundNumber((0.5197 - 0.0006 * stg), 5);

		double diff = measured - predicted;
		double zscore = roundNumber((diff / 0.1), 2);
		return new CalcResult("Ductus Venosus Preload Index",
				findPercentile(zscore), predicted);
	}

	// ductus venosus peak velocity index (S-a){}/D
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeDVPVI(int, double)
	 */
	@Override
	public CalcResult computeDVPVI(int days, double measured) {
		double stg = adjustDays(days);
		double predicted = roundNumber((0.592 - 0.0006 * stg), 5);
		double diff = measured - predicted;
		double sd = .12;
		double zscore = roundNumber((diff / sd), 2);
		return new CalcResult("ductus venosus peak velocity index",
				findPercentile(zscore), predicted);
	}

	// Kidney length
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeKIDLENGTH(int, double)
	 */
	@Override
	public CalcResult computeKIDNEY(int days, double measured) {

		double stg = adjustDays(days);
		double SA = stg / 10;
		double K0 = -1 * 1 * (5.0562 * Math.sqrt(SA));
		double KL1 = 5.4141 * Math.log(SA) + K0 + 6.3939;
		double predicted = roundNumber(Math.exp(KL1), 1);
		double SD = 0.1165;
		double zscore = (Math.log(measured) - (5.4141 * Math.log(SA) - 5.0562
				* Math.sqrt(SA) + 6.3939))
				/ SD;
		return new CalcResult("Kidney Length", findPercentile(zscore),
				predicted);
	}

	// mean renal pelvis antero-posterior diameter
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeRPELVIS(int, double)
	 */
	@Override
	public CalcResult computePELVIS(int days, double measured) {

		double stg = adjustDays(days);
		double predicted = roundNumber(0.098592 * stg + 0.19718, 1);
		double diff = measured - predicted;

		double SD = 1;
		double hi = roundNumber((.15054 * stg + 1.2151), 1);
		double lo = roundNumber((.054264 * stg - .015504), 1);

		double LOSD = (predicted - lo) / 1.28;
		double HISD = (hi - predicted) / 1.28;

		if (diff > 0) {
			SD = HISD;
		} else if (diff < 0) {
			SD = LOSD;
		}
		double zscore = roundNumber((diff / SD), 2);
		return new CalcResult("Pelvis antero-posterior diameter", findPercentile(zscore),
				measured);

	}

	// mean kidney antero-posterior diameter
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.biometry.IFetalBiometry#computeAPDIAM(int, double)
	 */
	@Override
	public CalcResult computeAPDIAM(int days, double measured) {

		double stg = adjustDays(days);
		double SA = stg / 10;
		double K0 = -1 * 1 * (4.8576 * Math.sqrt(SA));
		double KL1 = 5.0861 * Math.log(SA) + K0 + 5.9114;
		double predicted = roundNumber(Math.exp(KL1), 1);
		double zscore = (Math.log(measured) - (5.0861 * Math.log(SA) - 4.8576
				* Math.sqrt(SA) + 5.9114))
				/ (-0.0018793 * stg + 0.21256);

		return new CalcResult("kidney antero-posterior diameter", findPercentile(zscore),
				predicted);
	}
	
	public CalcResult performCalc(int days, double value, MetaCalc calc)
	{
		String methodName = calc.getMethodName();

		try {		
			
			java.lang.reflect.Method method = this.getClass().getMethod(methodName, int.class, double.class);
			return (CalcResult) method.invoke(this,days, value);
			
		} catch (SecurityException e) {
			throw new BiometryException("Security Problem executing " + methodName,e);
		} catch (NoSuchMethodException e) {
			throw new BiometryException("No such method " + methodName,e);
		} catch (IllegalArgumentException e) {
			throw new BiometryException("bad argument for " + methodName,e);
		} catch (IllegalAccessException e) {
			throw new BiometryException("bad access for " + methodName,e);
		} catch (InvocationTargetException e) {
			throw new BiometryException("bad target for " + methodName,e);
		}
	}


}
